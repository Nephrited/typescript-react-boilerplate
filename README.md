# typescript-react-boilerplate

This is an attempt at making a quickstart project base for react and typescript.

The project is not designed to be a catchall for any and every use, but rather just a collection of the things I need to setup a project quickly.
