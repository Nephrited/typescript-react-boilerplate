import * as React from "react";
import * as ReactDOM from "react-dom";
import "./index.scss";

ReactDOM.render(
	<div className="container">Hello World!</div>,
	document.getElementById("root"),
);
